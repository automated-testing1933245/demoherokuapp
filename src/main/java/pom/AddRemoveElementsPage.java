package pom;

import org.openqa.selenium.By;

public class AddRemoveElementsPage extends BasePage{

    public static final String url = "https://the-internet.herokuapp.com/add_remove_elements/";

    //selectors
    public By addElementButton = By.cssSelector("button[onclick='addElement()']");
}
