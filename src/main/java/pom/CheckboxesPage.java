package pom;

import org.openqa.selenium.By;

public class CheckboxesPage extends BasePage{
    public static final String url = "https://the-internet.herokuapp.com/add_remove_elements/";

    //selectors
    public By checkbox1 = By.cssSelector("input:nth-child(1)");
    public By checkbox2 = By.cssSelector("input:nth-child(3)");

    //verifications
    public boolean isCheckbox1Checked(){
        return driver.findElement(checkbox1).isSelected();
    }

    public boolean isCheckbox2Checked(){
        return driver.findElement(checkbox2).isSelected();
    }
}
